#!/bin/bash
#v1.0 (13/04/2021)

DIAGRAM_TEMPLATE="seba_diagram.tmpl"
DIAGRAM_OUT="seba_diagram.txt"

declare -a portlist=(
	"rg.uplink rg eth0"
	"onu.downlink onu eth1"
	"onu.uplink onu eth0"
	"olt.downlink olt eth0"
	"olt.uplink olt eth1"
	"agg.downlink app=mininet eth1"
	"agg.uplink app=mininet eth0"
)

if [ -f ${DIAGRAM_TEMPLATE} ]
then
	if [ -w ${DIAGRAM_OUT} ] || [ ! -f ${DIAGRAM_OUT} ]
	then
		cp ${DIAGRAM_TEMPLATE} ${DIAGRAM_OUT}
	else
		echo "Can't write to ${DIAGRAM_OUT}"
	fi
else
	echo "Missing diagram template file "
	exit -1
fi

for port in "${portlist[@]}"
do
	port=($port)
	template_label=${port[0]}
	kubernetes_label=${port[1]}
	container_if=${port[2]}
	docker_instance=$(kubectl get pod -l ${kubernetes_label} -o jsonpath='{.items[*].status.containerStatuses[*].containerID}' --all-namespaces)
	instance_pid=$(docker inspect -f '{{.State.Pid}}' ${docker_instance/docker:\/\//})
	container_ifindex=$(nsenter --target ${instance_pid} --net -- ip -br -j link show ${container_if} | jq '.[0].link_index')
	host_side_veth=$(ip -j link show | jq ".[] | select(.ifindex==${container_ifindex}) | .ifname")
	echo ${template_label}=${host_side_veth}
	sed -i -e "s/\${${template_label}}/${host_side_veth//\"/}/" ${DIAGRAM_OUT}
done
