#!/usr/bin/env python3
# find_netnsid_mappings.py
# Version: 1.0 (2018/05/28)
# Author: https://unix.stackexchange.com/users/288012/thediveo
# Ref: https://unix.stackexchange.com/questions/441876/how-to-find-the-network-namespace-of-a-veth-peer-ifindex

import psutil
import os
import pyroute2
from pyroute2.netlink import rtnl, NLM_F_REQUEST
from pyroute2.netlink.rtnl import nsidmsg
from nsenter import Namespace

# phase I: gather network namespaces from /proc/[0-9]*/ns/net
netns = dict()
for proc in psutil.process_iter():
    netnsref= '/proc/{}/ns/net'.format(proc.pid)
    if proc.pid == 1:
        is_root = True
    else:
        is_root = False
    try:
        netnsid = os.stat(netnsref).st_ino
        if netnsid in netns:
            if len(proc.cmdline()) > 0:
                netns[netnsid]['procs'].append(" ".join(proc.cmdline()))
        else:
            netns[netnsid] = { 'netnsref': netnsref, 'procs': [" ".join(proc.cmdline())], 'is_root': is_root }
    except FileNotFoundError:
        print('Broken or corrupted link for PID {}'.format(proc.pid))
        continue

# phase II: ask kernel "oracle" about the local IDs for the
# network namespaces we've discovered in phase I, doing this
# from all discovered network namespaces
for id, obj in netns.items():
    with Namespace(obj['netnsref'], 'net'):
        print('Inside namespace net:[{}] ...'.format(id))
        if obj['is_root']:
            print('  This is the root namespace (omitting process list)')
        else:
            print('  Processes:')
            for proc_name in obj['procs']:
                print('    {}'.format(proc_name))
        ipr = pyroute2.IPRoute()
        print('  Linked namespaces:')
        for netnsid, netnsobj in netns.items():
            with open(netnsobj['netnsref'], 'r') as netnsf:
                req = nsidmsg.nsidmsg()
                req['attrs'] = [('NETNSA_FD', netnsf.fileno())]
                resp = ipr.nlm_request(req, rtnl.RTM_GETNSID, NLM_F_REQUEST)
                local_nsid = dict(resp[0]['attrs'])['NETNSA_NSID']
            if local_nsid != 2**32-1:
                print('  net:[{}] <--> nsid {}'.format(netnsid, local_nsid))
    print()
