#!/usr/bin/env python3
# inspect_pod_net.py
# Version 1.0 (2021-04-27)

import argparse
import docker
import kubernetes
import nsenter
import os
import prettytable
import pyroute2

def pod_matches_labels(pod, label_list):
    """Checks whether a pod matches the given label list. Specified
    label list may provide values (label=value) or just names
    (label)"""

    for required_label in label_list:
        kv_label = required_label.split("=")
        required_label_name = kv_label[0]
        if required_label_name not in pod.metadata.labels:
            return False
        elif len(kv_label) == 2:
            required_label_value = kv_label[1]
            actual_label_value = pod.metadata.labels[required_label_name]
            if required_label_value != actual_label_value:
                return False
    return True

kubernetes.config.load_kube_config()
kube_client = kubernetes.client.CoreV1Api()
docker_client = docker.APIClient(base_url='unix://var/run/docker.sock')

parser = argparse.ArgumentParser(description=os.path.basename(__file__))
parser.add_argument('--namespace', '-n', help='Kubernetes namespace (\'default\' if not specified)')
parser.add_argument('--label', '-l', help='Pod label for selection')
#parser.add_argument('--help', '-h', help='Print command help and exit')
parser.add_argument('pod', help='Specific pod name', nargs='?')
args = parser.parse_args()

namespace = args.namespace or "default"
candidate_pod_list = kube_client.list_namespaced_pod(namespace)
actual_pod_list = []

if args.pod is None:
    if args.label is None:
        print("Bad syntax: Must specify either pod or label")
        exit(1)
    else:
        # Pontentially multiple pods matching label
        labels = args.label.split(",")
        for pod in candidate_pod_list.items:
            if pod_matches_labels(pod, labels):
                actual_pod_list.append(pod)
else:
    if args.label is None:
        # Single pod args.pod
        for pod in candidate_pod_list.items:
            if pod.metadata.name == args.pod:
                actual_pod_list.append(pod)
    else:
        print("Bad syntax: Can't specify both a pod name and a label at " \
              "the same time")
        exit(1)

host_iproute_db = pyroute2.NDB()
host_ifs = host_iproute_db.interfaces

for pod in actual_pod_list:
    container_instance = pod.status.container_statuses[0].container_id
    container_runtime, instance_id = container_instance.split("://")
    if container_runtime != "docker":
        print("Error: Unknown or unsupported container runtime {}".format(container_runtime))
    else:
        docker_container = docker_client.inspect_container(instance_id)
        instance_pid=docker_container['State']['Pid']
        print("Container interfaces for pod {} (Container ID: {}, " \
              "PID: {})".format(pod.metadata.name,
                                instance_id[:12],
                                instance_pid))
        table = prettytable.PrettyTable()
        table.field_names = ["IF", "IF Name", "MAC", "Host IF", "Host IF Name", "Host IF MAC"]
        table.align = "l"
        with nsenter.Namespace(instance_pid, 'net') as ns:
            ns_iproute_db = pyroute2.NDB()
            for if_id, if_data in ns_iproute_db.interfaces.items():
                linked_id_ref = "N/A"
                host_if_name = "N/A"
                host_if_addr = "N/A"
                if if_data['link'] is not None and if_data['link_netnsid'] is not None:
                    linked_id_ref = "{} (nsid {})".format(if_data['link'], if_data['link_netnsid'])
                    host_if_data = host_ifs[{'index': if_data['link']}]
                    host_if_name = host_if_data['ifname']
                    host_if_addr = host_if_data['address']
                table.add_row([
                    if_data['index'],
                    if_data['ifname'],
                    if_data['address'],
                    linked_id_ref,
                    host_if_name,
                    host_if_addr])
        print(table)
