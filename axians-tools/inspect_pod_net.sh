#!/bin/bash
# inspect_pod_net.sh
# Version 1.0 (26/04/2021)

#Print usage to stderr and exit
usage() {
  echo
  echo "Usage: $0 [-n <namespace>] <pod-name>"
  echo
  exit 1;
}

kube_namespace=default

#Get flags first
while getopts "n:" opt
do
  case "${opt}" in
    n)
      kube_namespace=${OPTARG}
      ;;
    *)
      usage
      ;;
  esac
done

#Now get positional arguments
shift $((OPTIND-1))
#We expect exactly 1 (<pod-name>)
[ $# -ne 1 ] && usage

pod_name="$1"

docker_instance=$(kubectl get pod ${pod_name} -o jsonpath='{.status.containerStatuses[*].containerID}' -n ${kube_namespace})
instance_pid=$(docker inspect -f '{{.State.Pid}}' ${docker_instance/docker:\/\//})
echo "Root PID for $pod_name is $instance_pid"
echo "Container to host interface mapping is as follows:"

container_ifs=$(nsenter -t ${instance_pid} -n ip -br -j link list | jq -r '.[]| {ifname,link_index}|@base64')

for container_if in ${container_ifs}
do
        container_if_obj=$(echo ${container_if} | base64 --decode)
        container_if_name=$(echo ${container_if_obj} | jq -r '.ifname')
        host_if_id=$(echo ${container_if_obj} | jq -r '.link_index')
        if [ -z "${host_if_id}" ] || [ "${host_if_id}" == "null" ]
        then
                echo "  ${pod_name}:${container_if_name} (internal)"
        else
                host_if_name=$(ip -j link list | jq -r ".[]|select(.ifindex==${host_if_id})|.ifname")
                echo "  ${pod_name}:${container_if_name} <=> ${host_if_name}"
        fi
done
